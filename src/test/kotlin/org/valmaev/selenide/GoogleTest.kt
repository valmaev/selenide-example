package org.valmaev.selenide

import org.testng.annotations.Test
import org.openqa.selenium.By
import com.codeborne.selenide.Selenide.*
import com.codeborne.selenide.Condition.text


class GoogleTest {

    @Test
    fun userCanSearchKeywordWithGoogle() {
        open("http://google.com")
        `$`(By.name("q")).setValue("Selenide").pressEnter()
        `$$`("#ires h3.r").shouldHaveSize(10)
        `$`("#ires h3.r").shouldHave(text("Selenide"))
    }

    @Test
    fun testThatShouldFail() {
        open("http://google.com")
        `$`("#non-existent-id").shouldHave(text("foobar"))
    }
}
